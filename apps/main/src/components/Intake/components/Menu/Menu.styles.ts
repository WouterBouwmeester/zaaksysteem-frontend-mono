// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { makeStyles } from '@material-ui/core';
import { Theme } from '@mintlab/ui/types/Theme';

export const useMenuStyles = makeStyles(
  ({ palette: { elephant }, mintlab: { greyscale } }: Theme) => {
    return {
      buttons: {
        color: elephant.dark,
        display: 'flex',
        alignItems: 'center',
        '&>div': {
          width: 'auto',
          whiteSpace: 'nowrap',
        },
      },
      textButton: {
        backgroundColor: greyscale.dark,
        boxShadow: 'none',
        marginRight: 14,
        '&:hover': {
          boxShadow: 'none',
        },
      },
    };
  }
);
