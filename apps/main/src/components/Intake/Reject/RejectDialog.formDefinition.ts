// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import * as i18next from 'i18next';
import * as fieldTypes from '@zaaksysteem/common/src/components/form/constants/fieldTypes';
import { SingleStepFormDefinition } from '@zaaksysteem/common/src/components/form/types/formDefinition.types';
import { FormValuesType } from './RejectDialog.types';

export const getFormDefinition = (
  t: i18next.TFunction
): SingleStepFormDefinition<FormValuesType> => [
  {
    name: 'reason',
    type: fieldTypes.TEXT,
    value: null,
    required: true,
    placeholder: t('rejectDocument.reason'),
    label: t('rejectDocument.reason'),
    translations: {
      'form:choose': t('assignDocument.role.placeholder'),
    },
  },
];
