// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import { ADDRESS_BOX } from '@zaaksysteem/common/src/components/form/constants/fieldTypes';
import { useForm } from '@zaaksysteem/common/src/components/form/hooks/useForm';

const Demo = () => {
  const { fields } = useForm({
    formDefinition: [
      {
        name: 'location',
        type: ADDRESS_BOX,
        required: false,
        label: 'Address box field',
        placeholder: 'placeholder',
        readOnly: false,
        value: null,
      },
    ],
  });

  return (
    <React.Fragment>
      {fields.map(({ FieldComponent, label, value, name, ...rest }) => {
        return (
          <div key={name} style={{ padding: 10, width: 650 }}>
            <h3>{label}</h3>
            <div style={{ height: 400, width: 650 }}>
              {<FieldComponent {...rest} name={name} value={value} />}
            </div>
            {<span>Value: {JSON.stringify(value)}</span>}
          </div>
        );
      })}
    </React.Fragment>
  );
};

export default Demo;
