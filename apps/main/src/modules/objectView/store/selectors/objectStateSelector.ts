// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { ObjectViewRootStateType } from '../objectView.reducer';
import { ObjectViewStateType } from '../objectView.reducer';

export const objectStateSelector = (
  state: ObjectViewRootStateType
): ObjectViewStateType => state.objectView;
