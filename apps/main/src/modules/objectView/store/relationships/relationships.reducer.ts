// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { Reducer } from 'redux';
import { AjaxAction } from '@zaaksysteem/common/src/library/redux/ajax/createAjaxAction';
import {
  AJAX_STATE_INIT,
  AjaxState,
} from '@zaaksysteem/common/src/library/redux/ajax/createAjaxConstants';
import { APICaseManagement } from '@zaaksysteem/generated';
import { handleAjaxStateChange } from '@zaaksysteem/common/src/library/redux/ajax/handleAjaxStateChange';
import {
  RelatedCaseRowType,
  RelatedObjectRowType,
  RelatedSubjectRowType,
} from '../../types/ObjectView.types';
import {
  FETCH_CASES,
  FETCH_OBJECTS,
  FETCH_SUBJECTS,
} from './relationships.constants';
import { PayloadType } from './relationships.actions';

export interface RelationshipsRootStateType {
  cases: {
    state: AjaxState;
    cases: RelatedCaseRowType[];
  };
  objects: {
    state: AjaxState;
    objects: RelatedObjectRowType[];
  };
  subjects: {
    state: AjaxState;
    subjects: RelatedSubjectRowType[];
  };
}

const handleCasesStateChange = handleAjaxStateChange(FETCH_CASES);
const handleObjectsStateChange = handleAjaxStateChange(FETCH_OBJECTS);
const handleSubjectsStateChange = handleAjaxStateChange(FETCH_SUBJECTS);

const initialState: RelationshipsRootStateType = {
  cases: {
    state: AJAX_STATE_INIT,
    cases: [],
  },
  objects: {
    state: AJAX_STATE_INIT,
    objects: [],
  },
  subjects: {
    state: AJAX_STATE_INIT,
    subjects: [],
  },
};

/* eslint complexity: [2, 12] */
const handleCasesFetchSuccess = (
  state: RelationshipsRootStateType['cases'],
  action: AjaxAction<APICaseManagement.GetRelatedCasesResponseBody, PayloadType>
) => {
  const data = action.payload.response.data;

  if (!data || data.length === 0) {
    return handleCasesStateChange(state, action);
  }

  return handleCasesStateChange(
    {
      ...state,
      cases: data.map(row => ({
        uuid: row.id || '',
        name: row.id || '',
        nr: row.attributes?.number || 0,
        progress: (row.attributes?.progress || 0) * 100,
        status: row.attributes?.status || '',
        caseType: row.relationships?.case_type?.meta?.summary || '',
        result: row.attributes?.result || '',
        extra: row.meta?.summary || '',
        assignee: row.relationships?.assignee?.meta?.summary || '',
      })),
    },
    action
  );
};

/* eslint complexity: [2, 12] */
const handleObjectsFetchSuccess = (
  state: RelationshipsRootStateType['objects'],
  action: AjaxAction<
    APICaseManagement.GetRelatedObjectsResponseBody,
    PayloadType
  >
) => {
  const data = action.payload.response.data;

  if (!data || data.length === 0) {
    return handleObjectsStateChange(state, action);
  }

  return handleObjectsStateChange(
    {
      ...state,
      objects: data.map((row: any) => ({
        uuid: row.id || '',
        name: row.meta?.summary || '',
        objectType: row.relationships?.object_type?.meta.summary,
        externalReference: 'test',
      })),
    },
    action
  );
};

/* eslint complexity: [2, 12] */
const handleSubjectsFetchSuccess = (
  state: RelationshipsRootStateType['subjects'],
  action: AjaxAction<
    APICaseManagement.GetRelatedSubjectsResponseBody,
    PayloadType
  >
) => {
  const data = action.payload.response.data;

  if (!data || data.length === 0) {
    return handleSubjectsStateChange(state, action);
  }

  return handleSubjectsStateChange(
    {
      ...state,
      subjects: data.map(row => ({
        uuid: row.id || '',
        name: row.meta?.summary || '',
        subjectType: row.attributes?.type,
        roleType: row.attributes?.roles,
      })),
    },
    action
  );
};

export const relationships: Reducer<
  RelationshipsRootStateType,
  AjaxAction<unknown>
> = (state = initialState, action) => {
  switch (action.type) {
    case FETCH_CASES.PENDING:
    case FETCH_CASES.ERROR:
      return {
        ...state,
        cases: handleCasesStateChange(state.cases, action as AjaxAction<{}>),
      };
    case FETCH_CASES.SUCCESS:
      return {
        ...state,
        cases: handleCasesFetchSuccess(
          state.cases,
          action as AjaxAction<
            APICaseManagement.GetRelatedCasesResponseBody,
            PayloadType
          >
        ),
      };

    case FETCH_OBJECTS.PENDING:
    case FETCH_OBJECTS.ERROR:
      return {
        ...state,
        objects: handleObjectsStateChange(
          state.objects,
          action as AjaxAction<{}>
        ),
      };
    case FETCH_OBJECTS.SUCCESS:
      return {
        ...state,
        objects: handleObjectsFetchSuccess(
          state.objects,
          action as AjaxAction<
            APICaseManagement.GetRelatedObjectsResponseBody,
            PayloadType
          >
        ),
      };

    case FETCH_SUBJECTS.PENDING:
    case FETCH_SUBJECTS.ERROR:
      return {
        ...state,
        subjects: handleSubjectsStateChange(
          state.subjects,
          action as AjaxAction<{}>
        ),
      };
    case FETCH_SUBJECTS.SUCCESS:
      return {
        ...state,
        subjects: handleSubjectsFetchSuccess(
          state.subjects,
          action as AjaxAction<
            APICaseManagement.GetRelatedSubjectsResponseBody,
            PayloadType
          >
        ),
      };
    default:
      return state;
  }
};

export default relationships;
