// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { createAjaxAction } from '@zaaksysteem/common/src/library/redux/ajax/createAjaxAction';
import { buildUrl } from '@mintlab/kitchen-sink/source';
import { APICaseManagement } from '@zaaksysteem/generated';
import { FETCH_OBJECT_TYPE } from './objectType.constants';

const fetchObjectAction = createAjaxAction(FETCH_OBJECT_TYPE);

export type FetchObjectTypeActionPayloadType = {
  uuid: string;
};

export const fetchObjectType = (uuid: string) =>
  fetchObjectAction<FetchObjectTypeActionPayloadType>({
    url: buildUrl<APICaseManagement.GetCustomObjectTypeRequestParams>(
      `/api/v2/cm/custom_object_type/get_custom_object_type`,
      {
        uuid,
      }
    ),
    method: 'GET',
  });
