// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { SessionRootStateType } from '@zaaksysteem/common/src/store/session/session.reducer';

export const sessionSelector = (state: SessionRootStateType) =>
  state.session.data;
