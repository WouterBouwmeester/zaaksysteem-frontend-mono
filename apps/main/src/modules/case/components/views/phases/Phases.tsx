// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React, { useState } from 'react';
import { PanelLayout } from '../../../../../components/PanelLayout/PanelLayout';
import { Panel } from '../../../../../components/PanelLayout/Panel';
import { CaseObjType, CaseTypeType } from '../../../Case.types';
import { useCasePhasesStyles } from './Phases.styles';
import Form from './Form/Form';
import Navigation from './Navigation/Navigation';
import SideBar from './SideBar/SideBar';
import { getNavigationItems } from './Phases.library';

export type PhasesPropsType = {
  rootPath: string;
  pathWithoutPhaseNumber: string;
  caseObj: CaseObjType;
  caseType: CaseTypeType;
  phaseNumber: number;
};

const Phases: React.FunctionComponent<PhasesPropsType> = ({
  rootPath,
  pathWithoutPhaseNumber,
  caseObj,
  caseType,
  phaseNumber,
}) => {
  const classes = useCasePhasesStyles();
  const [openTasksCount, setOpenTasksCount] = useState<number>();
  const [checkedActionsCount, setCheckedActionsCount] = useState<number>();

  return (
    <div className={classes.wrapper}>
      <Navigation
        items={getNavigationItems(
          pathWithoutPhaseNumber,
          caseObj,
          caseType,
          phaseNumber
        )}
      />
      <div className={classes.content}>
        <PanelLayout>
          <Panel className={classes.form}>
            <Form
              caseObj={caseObj}
              caseType={caseType}
              phaseNumber={phaseNumber}
            />
          </Panel>
          <Panel type="side" className={classes.sideBar}>
            <SideBar
              rootPath={rootPath}
              caseObj={caseObj}
              caseType={caseType}
              phaseNumber={phaseNumber}
              openTasksCount={openTasksCount}
              checkedActionsCount={checkedActionsCount}
              setOpenTasksCount={setOpenTasksCount}
              setCheckedActionsCount={setCheckedActionsCount}
            />
          </Panel>
        </PanelLayout>
      </div>
    </div>
  );
};

export default Phases;
