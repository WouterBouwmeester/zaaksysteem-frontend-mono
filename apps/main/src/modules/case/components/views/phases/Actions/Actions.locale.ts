// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

export default {
  nl: {
    noActions: 'Er zijn geen acties voor deze fase.',
    checkboxTooltip: 'Automatisch uitvoeren bij faseovergang',
    titleTooltip: '{{noun}} bewerken of direct {{verb}}',
    case: {
      noun: {
        deelzaak: 'Deelzaak',
        gerelateerd: 'Gerelateerde zaak',
        vervolgzaak_datum: 'Vervolgzaak',
        vervolgzaak: 'Vervolgzaak',
      },
      verb: 'starten',
    },
    template: {
      noun: 'Sjabloon',
      verb: 'aanmaken',
    },
    email: {
      noun: 'Bericht',
      verb: 'versturen',
    },
    object_action: {
      noun: 'Object',
      verb: 'aanmaken',
    },
    allocation: {
      noun: 'Toewijziging',
      verb: 'wijzigen',
    },
  },
};
