// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import { Switch, Route, Redirect } from 'react-router-dom';
import I18nResourceBundle from '@zaaksysteem/common/src/components/i18nResourceBundle/I18nResourceBundle';
import Phases, { PhasesPropsType } from './Phases';
import locale from './Phases.locale';

type PhasesViewPropsType = Pick<
  PhasesPropsType,
  'rootPath' | 'caseObj' | 'caseType'
>;

const PhasesView: React.ComponentType<PhasesViewPropsType> = ({
  rootPath,
  caseObj,
  caseType,
}) => (
  <I18nResourceBundle resource={locale} namespace="casePhases">
    <Switch>
      <Redirect
        exact
        from={`${rootPath}`}
        to={`${rootPath}/${caseObj.phase}`}
      />
      <Route
        path={`${rootPath}/:phaseNumber`}
        render={({ match }) => (
          <Phases
            rootPath={match.url}
            pathWithoutPhaseNumber={rootPath}
            phaseNumber={Number(match.params.phaseNumber)}
            caseObj={caseObj}
            caseType={caseType}
          />
        )}
      />
    </Switch>
  </I18nResourceBundle>
);

export default PhasesView;
