// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import Icon, { iconNames } from '@mintlab/ui/App/Material/Icon';
//@ts-ignore
import ZsIcon from '@mintlab/ui/App/Zaaksysteem/ZsIcon';
import { useIconsStyles } from './Icons.styles';

export const Allocation: React.ComponentType = () => (
  <Icon size="small" classes={{ root: useIconsStyles()['allocation'] }}>
    {iconNames.people}
  </Icon>
);

export const Case: React.ComponentType = () => (
  <ZsIcon size="small">{'entityType.case'}</ZsIcon>
);

export const Email: React.ComponentType = () => (
  <ZsIcon size="small">{'entityType.email_template'}</ZsIcon>
);

export const Object_action: React.ComponentType = () => (
  <ZsIcon size="small">{'entityType.object_type'}</ZsIcon>
);

export const Template: React.ComponentType = () => (
  <ZsIcon size="small">{'entityType.document_template'}</ZsIcon>
);

const Icons: {
  allocation: React.ComponentType;
  case: React.ComponentType;
  email: React.ComponentType;
  object_action: React.ComponentType;
  template: React.ComponentType;
} = {
  allocation: Allocation,
  case: Case,
  email: Email,
  object_action: Object_action,
  template: Template,
};

export default Icons;
