// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import { useTranslation } from 'react-i18next';
import { Link } from 'react-router-dom';
import Icon, { iconNames } from '@mintlab/ui/App/Material/Icon';
import {
  SideMenu,
  SideMenuItemType,
} from '@mintlab/ui/App/Zaaksysteem/SideMenu';
import { CaseObjType } from '../../../Case.types';

const getSelectedItem = (rootPath: string): string => {
  const url = window.location.toString();
  const path = url.replace(rootPath, '');
  const view = path.split('/')[3];

  return view || 'phases';
};

const MenuItemLink = React.forwardRef<any, SideMenuItemType>(
  ({ href, ...restProps }, ref) => <Link to={href || ''} {...restProps} />
);

const Navigation: React.ComponentType<{
  rootPath: string;
  caseObj: CaseObjType;
  folded: boolean;
}> = ({ rootPath, caseObj, folded }) => {
  const [t] = useTranslation('case');
  const selectedItem = getSelectedItem(rootPath);

  const menuItems: SideMenuItemType[] = [
    { id: 'phases', icon: iconNames.done_all },
    { id: 'documents', icon: iconNames.insert_drive_file },
    { id: 'timeline', icon: iconNames.schedule },
    { id: 'communication', icon: iconNames.chat_bubble },
    ...(caseObj.location ? [{ id: 'location', icon: iconNames.map }] : []),
    { id: 'relations', icon: iconNames.link },
  ].map<SideMenuItemType>(({ id, icon }) => {
    const label = t(`views.${id}`);

    return {
      id,
      label: folded ? undefined : label,
      href: `${rootPath}/${id}`,
      tooltip: label,
      selected: selectedItem === id,
      icon: <Icon size="small">{icon}</Icon>,
      component: MenuItemLink,
    };
  });

  return <SideMenu items={menuItems} />;
};

export default Navigation;
