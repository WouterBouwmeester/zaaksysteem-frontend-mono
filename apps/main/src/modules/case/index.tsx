// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React, { useState, useEffect } from 'react';
import { useSelector } from 'react-redux';
import { Loader } from '@mintlab/ui/App/Zaaksysteem/Loader';
import I18nResourceBundle from '@zaaksysteem/common/src/components/i18nResourceBundle/I18nResourceBundle';
import { sessionSelector } from './Case.selectors';
import locale from './Case.locale';
import Case from './Case';
import {
  redirectToCaseUuid,
  getCaseObj,
  getCaseType,
  getJobs,
} from './Case.library';
import { CaseObjType, CaseTypeType, JobType } from './Case.types';

const CaseModule: React.FunctionComponent<{
  rootPath: string;
  caseUuid: string;
}> = ({ rootPath, caseUuid }) => {
  const session = useSelector(sessionSelector);
  const [caseObj, setCaseObj] = useState<CaseObjType>();
  const [caseType, setCaseType] = useState<CaseTypeType>();
  const [jobs, setJobs] = useState<JobType[]>();

  useEffect(() => {
    // allow users to easily navigate to /main/case/123 while the new case view is being developed
    if (!isNaN(Number(caseUuid))) {
      redirectToCaseUuid(caseUuid);
      return;
    }

    if (!caseObj) {
      getCaseObj(caseUuid, setCaseObj);
    }

    if (!jobs) {
      getJobs(caseUuid, setJobs);
    }

    if (caseObj && !caseType) {
      getCaseType(caseObj, setCaseType);
    }
  }, [caseObj]);

  if (!caseObj || !caseType || !session || !jobs) {
    return <Loader />;
  }

  const refreshCaseObj = () => getCaseObj(caseUuid, setCaseObj);

  return (
    <I18nResourceBundle resource={locale} namespace="case">
      <Case
        session={session}
        caseObj={caseObj}
        refreshCaseObj={refreshCaseObj}
        caseType={caseType}
        jobs={jobs}
        rootPath={rootPath}
      />
    </I18nResourceBundle>
  );
};

export default CaseModule;
