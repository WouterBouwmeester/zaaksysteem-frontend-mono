// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import { DynamicModuleLoader } from 'redux-dynamic-modules-react';
import { getDashboardModule } from './store/dashboard.module';
import Dashboard from './components/Dashboard';
import { DashboardPropsType } from './components/Dashboard';
import ExternalUrl from './components/widgets/ExternalUrl/ExternalUrl';

const DashboardModule: React.ComponentType<DashboardPropsType> = ({
  match,
}) => {
  return window.location.href.includes('/widgets/') ? (
    <DynamicModuleLoader modules={[getDashboardModule()]}>
      <Dashboard match={match} />
    </DynamicModuleLoader>
  ) : (
    <>
      <h3>Dashboard View</h3>
      <ExternalUrl />
    </>
  );
};

export default DashboardModule;
