// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import Button from '@mintlab/ui/App/Material/Button';
import { useWidgetHeaderStyles } from '../Widget.style';
import { deleteWidgetPostMessage } from '../Widget.library';

type WidgetHeaderPropsType = {
  title: string;
  widgetUuid?: string;
  children?: React.ReactChild;
};

const WidgetHeader: React.ComponentType<WidgetHeaderPropsType> = ({
  title,
  widgetUuid,
  children,
}) => {
  const classes = useWidgetHeaderStyles();

  return (
    <div className={classes.widgetHeader}>
      <div className={classes.title}>{title}</div>
      {children}
      {widgetUuid && (
        <Button
          className={classes.delete}
          presets={['icon', 'small']}
          action={() => deleteWidgetPostMessage(widgetUuid)}
        >
          close
        </Button>
      )}
    </div>
  );
};

export default WidgetHeader;
