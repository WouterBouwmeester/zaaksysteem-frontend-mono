// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { StyleSheetCreatorType } from '@mintlab/ui/App/Zaaksysteem/Select/types/SelectStyleSheetType';

export const multiValueLabelStyles: StyleSheetCreatorType = ({
  theme: {
    mintlab: { greyscale, radius },
  },
}) => {
  return {
    container(base) {
      return {
        ...base,
      };
    },
    control(base) {
      return {
        ...base,
        border: 0,
        boxShadow: 'none',
      };
    },
    multiValue(base) {
      return {
        ...base,
        alignItems: 'center',
        background: greyscale.lighter,
        border: `1px solid ${greyscale.evenDarker}`,
        borderRadius: radius.chip,
        color: greyscale.darkest,
        display: 'flex',
        marginRight: 5,
        minWidth: 'auto',
        paddingLeft: 5,
        flex: '0 0 auto',
      };
    },
    multiValueRemove(base) {
      return {
        ...base,
        color: greyscale.evenDarker,
        '&:hover': {
          background: 'transparent',
          color: greyscale.darkest,
        },
      };
    },
    indicatorsContainer() {
      return {
        display: 'none',
      };
    },
  };
};

export const widgetHeaderMultiStyles: StyleSheetCreatorType = () => {
  return {
    container(base) {
      return {
        ...base,
        display: 'flex',
        flexWrap: 'nowrap',
        marginRight: 10,
        overflow: 'auto',
      };
    },
    control(base) {
      return {
        ...base,
        border: 0,
        boxShadow: 'none',
        '> div:first-child': {
          display: 'flex',
          flexWrap: 'nowrap',
          padding: 0,
        },
      };
    },
  };
};
