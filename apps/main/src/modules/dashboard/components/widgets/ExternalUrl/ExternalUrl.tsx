// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import { useTranslation } from 'react-i18next';
import I18nResourceBundle from '@zaaksysteem/common/src/components/i18nResourceBundle/I18nResourceBundle';
import Widget from '../Widget';
import WidgetHeader from '../components/WidgetHeader';
import locale from './ExternalUrl.locale';
import ExternalUrlEditView from './ExternalUrlEditView';

const ExternalUrl = () => {
  const [t] = useTranslation();
  const [editMode, setEditMode] = React.useState(true);
  const [settingValues, setSettingValues] = React.useState({
    title: '',
    url: '',
  });

  return (
    <Widget
      header={
        <WidgetHeader
          title={editMode ? t('externalUrl:titleEdit') : settingValues.title}
        />
      }
    >
      <>
        {editMode ? (
          <ExternalUrlEditView
            submit={values => {
              setEditMode(false);
              setSettingValues(values);
            }}
            t={t}
          />
        ) : (
          <iframe
            title={settingValues.title}
            src={settingValues.url}
            frameBorder="0"
          />
        )}
      </>
    </Widget>
  );
};

const ExternalUrlModule = () => (
  <I18nResourceBundle resource={locale} namespace="externalUrl">
    <ExternalUrl />
  </I18nResourceBundle>
);

export default ExternalUrlModule;
