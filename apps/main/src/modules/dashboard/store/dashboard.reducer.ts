// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { Reducer, AnyAction } from 'redux';

export interface DashboardStateType {}

export interface DashboardRootStateType {
  dashboard: DashboardStateType;
}

const initialState: DashboardStateType = {};

export const dashboardReducer: Reducer<DashboardStateType, AnyAction> = (
  state = initialState
) => {
  return state;
};
