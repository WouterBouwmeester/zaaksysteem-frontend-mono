// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import Common from '../tables/common/Common';
import { ObjectType, ObjectTypeType } from '../Information.types';
import Notifications from '../tables/notificationSettings/NotificationSettings';
import Signature from '../tables/signature/Signature';
import PhoneExtension from '../tables/phoneExtension/PhoneExtension';
import RelatedObject from '../tables/relatedObject/RelatedObject';
import Special from '../tables/special/Special';
import { getCommonFormDefinition } from '../tables/common/common.formDefinition.employee';
import { getSpecialFormDefinition } from '../tables/special/special.formDefinition.employee';
import { SubjectType, SessionType } from './../../ContactView.types';

export interface PersonViewPropsType {
  data: {
    subject: SubjectType;
    object: ObjectType;
    objectType: ObjectTypeType;
    hasPhoneIntegration: boolean;
  };
  session: SessionType;
  refreshSubject: () => {};
  setSnackOpen: any;
}

const PersonView: React.FunctionComponent<PersonViewPropsType> = ({
  data: { subject, object, objectType, hasPhoneIntegration },
  session: { loggedInUserRoles, signatureUploadRole, loggedInUserUuid },
  refreshSubject,
  setSnackOpen,
}) => {
  const lookingAtSelf = loggedInUserUuid === subject.uuid;

  return (
    <>
      <Common
        subject={subject}
        getFormDefinition={getCommonFormDefinition}
        refreshSubject={refreshSubject}
        setSnackOpen={setSnackOpen}
      />
      {lookingAtSelf && <Notifications subject={subject} />}
      {lookingAtSelf && hasPhoneIntegration && (
        <PhoneExtension subject={subject} />
      )}
      <Signature
        subject={subject}
        loggedInUserRoles={loggedInUserRoles}
        signatureUploadRole={signatureUploadRole}
        lookingAtSelf={lookingAtSelf}
      />
      {object && <RelatedObject object={object} objectType={objectType} />}
      <Special subject={subject} getFormDefinition={getSpecialFormDefinition} />
    </>
  );
};

export default PersonView;
