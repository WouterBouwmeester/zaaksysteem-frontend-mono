// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import { useInformationStyles } from './Information.style';

export const SeperatorLabel = ({ str }: { str?: String }) => {
  const classes = useInformationStyles();
  return <div className={classes.formLabel}>{str}</div>;
};
