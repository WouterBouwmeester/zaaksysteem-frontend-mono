// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import {
  EMAIL,
  SELECT,
  TEXT,
} from '@zaaksysteem/common/src/components/form/constants/fieldTypes';
import * as validationRules from './validationRules';

export default {
  [EMAIL]: validationRules.emailRule,
  [SELECT]: validationRules.selectRule,
  [TEXT]: validationRules.textRule,
};
