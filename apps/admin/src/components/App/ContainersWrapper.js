// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React, { Fragment } from 'react';
import SnackbarRenderer from '@zaaksysteem/common/src/components/SnackbarRenderer/SnackbarRenderer';
import DialogRendererContainer from '../DialogRenderer/DialogRendererContainer';

/**
 * @param {Object} props
 * @param {Object} props.children
 * @return {ReactElement}
 */
const ContainersWrapper = ({ children }) => (
  <Fragment>
    {children}
    <DialogRendererContainer />
    <SnackbarRenderer />
  </Fragment>
);

export default ContainersWrapper;
