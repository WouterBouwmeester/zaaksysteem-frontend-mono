// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { ui } from './ui.reducer';

export const getUIModule = () => ({
  id: 'ui',
  reducerMap: {
    ui,
  },
});

export default getUIModule;
