// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import * as i18next from 'i18next';
import {
  CHECKBOX_GROUP,
  TEXTAREA,
} from '@zaaksysteem/common/src/components/form/constants/fieldTypes';
import { FormDefinitionStep } from '@zaaksysteem/common/src/components/form/types';
import { ObjectTypeFormShapeType } from '../../ObjectTypeForm.types';

export function getAuditStep(
  t: i18next.TFunction
): FormDefinitionStep<ObjectTypeFormShapeType> {
  return {
    title: t('objectTypeManagement:form.steps.audit.title'),
    fields: [
      {
        name: 'components_changed',
        type: CHECKBOX_GROUP,
        value: [],
        applyBackgroundColor: false,
        choices: [
          {
            label: t(
              'objectTypeManagement:form.fields.components_changed.choices.generic'
            ),
            value: 'attributes',
          },
          {
            label: t(
              'objectTypeManagement:form.fields.components_changed.choices.attributes'
            ),
            value: 'custom_fields',
          },
          {
            label: t(
              'objectTypeManagement:form.fields.components_changed.choices.relations'
            ),
            value: 'relationships',
          },
          {
            label: t(
              'objectTypeManagement:form.fields.components_changed.choices.authorizations'
            ),
            value: 'authorizations',
          },
        ],
        required: true,
        label: t('objectTypeManagement:form.fields.components_changed.label'),
      },
      {
        name: 'changes',
        type: TEXTAREA,
        isMultiline: true,
        value: '',
        required: true,
        label: t('objectTypeManagement:form.fields.changes.label'),
        help: t('objectTypeManagement:form.fields.changes.help'),
        placeholder: t('objectTypeManagement:form.fields.changes.placeholder'),
      },
    ],
  };
}
