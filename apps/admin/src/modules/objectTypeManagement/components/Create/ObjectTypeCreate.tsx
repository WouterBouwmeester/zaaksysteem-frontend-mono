// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import ObjectTypeForm from '../ObjectTypeForm/ObjectTypeForm';
import { createStateSelector } from '../../store/selectors/createStateSelector';
import { unsavedChangesSelector } from '../../store/selectors/unsavedChangesSelector';
import { ObjectTypeFormShapeType } from '../ObjectTypeForm/ObjectTypeForm.types';
import { createObjectTypeAction } from '../../store/create/create.actions';
import { setObjectTypeUnsavedChangesAction } from '../../store/unsavedChanges/unsavedChanges.actions';
import { useObjectTypeCreateStyle } from './ObjectTypeCreate.style';

export type ObjectTypeCreatePropsType = {
  folderUuid?: string;
};

const ObjectTypeCreate: React.ComponentType<ObjectTypeCreatePropsType> = ({
  folderUuid,
}) => {
  const { state } = useSelector(createStateSelector);
  const unsavedChanges = useSelector(unsavedChangesSelector);
  const dispatch = useDispatch();
  const classes = useObjectTypeCreateStyle();
  const handleSubmit = (values: ObjectTypeFormShapeType) =>
    dispatch(
      createObjectTypeAction({
        ...values,
        folderUuid,
      })
    );

  const handleChange = () =>
    !unsavedChanges && dispatch(setObjectTypeUnsavedChangesAction(true));

  return (
    <div className={classes.wrapper}>
      <ObjectTypeForm
        onSubmit={handleSubmit}
        onChange={handleChange}
        busy={state === 'pending'}
      />
    </div>
  );
};

export default ObjectTypeCreate;
