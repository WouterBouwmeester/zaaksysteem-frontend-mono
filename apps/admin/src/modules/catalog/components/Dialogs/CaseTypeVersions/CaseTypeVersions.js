// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React, { Component } from 'react';
import { unique } from '@mintlab/kitchen-sink/source';
import { withStyles } from '@material-ui/styles';
import { Dialog, DialogTitle } from '@mintlab/ui/App/Material/Dialog';
import { Loader } from '@mintlab/ui/App/Zaaksysteem/Loader';
import Render from '@mintlab/ui/App/Abstract/Render';
import CaseTypeVersion from './Components/CaseTypeVersion';
import { caseTypeVersionsStyleSheet } from './CaseTypeVersions.style';

/**
 * @return {ReactElement}
 */
class CaseTypeVersions extends Component {
  render() {
    const {
      t,
      classes,
      hide,
      initCaseTypeVersionsActivate,
      loading,
      case_type_id,
      versions,
    } = this.props;

    const title = t('caseTypeVersions:dialog.title');
    const labelId = unique();

    return (
      <Dialog
        disableBackdropClick={true}
        id="catalog-case-type-versions-dialog"
        open={true}
        title={title}
        onClose={() => hide()}
        scope="catalog-case-type-versions-dialog"
      >
        <DialogTitle
          elevated={true}
          icon="history"
          id={labelId}
          title={title}
          onCloseClick={() => hide()}
        />

        <Render condition={loading}>
          <Loader />
        </Render>

        <Render condition={!loading}>
          <div className={classes.dialogContent}>
            {versions.map((version, index) => (
              <CaseTypeVersion
                key={index}
                initCaseTypeVersionsActivate={initCaseTypeVersionsActivate}
                case_type_id={case_type_id}
                version={version}
                t={t}
              />
            ))}
          </div>
        </Render>
      </Dialog>
    );
  }
}

export default withStyles(caseTypeVersionsStyleSheet)(CaseTypeVersions);
