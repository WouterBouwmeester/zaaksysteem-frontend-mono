// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

/**
 * @param {Object} theme
 * @return {JSS}
 */
export const versionsStylesheet = ({
  mintlab: { greyscale },
  palette: { common },
  typography,
}) => ({
  main: {
    padding: '0px',
  },
  mainDetails: {
    padding: '0px',
    width: '100%',
    flexFlow: 'column',
  },
  version: {
    width: '100%',
    boxSizing: 'border-box',
  },
  versionExpanded: {
    margin: '0px',
  },

  expandButton: {
    '&&': {
      transform: 'translateY(-50%)',
    },
  },
  versionSummary: {
    alignItems: 'center',
  },
  versionNr: {
    marginRight: '20px',
    backgroundColor: greyscale.darker,
    borderRadius: '50%',
    width: '30px',
    height: '30px',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    color: common.black,
    fontWeight: typography.fontWeightMedium,
  },
  versionTitle: {
    '& span': {
      fontWeight: typography.fontWeightMedium,
    },
  },
  versionDetails: {
    backgroundColor: greyscale.light,
    borderRadius: '6px',
    padding: '20px',
    margin: '0px 20px 20px 20px',
    flexFlow: 'column',
  },
  versionDetailRow: {
    display: 'flex',
    margin: '10px 0px 10px 0px',
    '&>*:nth-child(1)': {
      width: '140px',
    },
    '&>*:nth-child(2)': {
      flex: 1,
      fontWeight: typography.fontWeightMedium,
    },
  },
  separator: {
    height: '1px',
    backgroundColor: greyscale.dark,
  },
  option: {
    marginBottom: '10px',
  },
  disabled: {
    opacity: '0.3',
  },
});
