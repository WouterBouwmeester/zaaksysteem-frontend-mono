// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { createAjaxConstants } from '../../../../library/redux/ajax/createAjaxConstants';

export const CATALOG_FOLDER_INIT = 'CATALOG:FOLDER:INIT';
export const CATALOG_FOLDER_SAVE = createAjaxConstants('CATALOG:FOLDER:SAVE');
