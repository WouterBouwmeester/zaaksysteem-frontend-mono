// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
/* global Formio, FormioUtils  */

let nonce;

document.createElement = (function (create) {
  return function () {
    var ret = create.apply(this, arguments);
    if (ret.tagName.toLowerCase() === 'style') {
      nonce =
        nonce ||
        document
          .querySelector('[property="csp-nonce"]')
          .getAttribute('content');
      ret.setAttribute('nonce', nonce);
    }
    return ret;
  };
})(document.createElement);

if (window.trustedTypes && window.trustedTypes.createPolicy) {
  window.trustedTypes.createPolicy('default', {
    createHTML: function (string) {
      return string;
    },
    createScriptURL: function (string) {
      return string;
    },
    createScript: function (string) {
      return string;
    },
  });
}

Formio.icons = 'fontawesome';
FormioUtils.Evaluator.noeval = true;

const loader = document.querySelector('.loader');
const selectionStep = document.querySelector('.selectionStep');
const startButton = document.querySelector('.startButton');
const nextStepButton = document.querySelector('.nextButton');
const templatesEl = document.querySelector('.templates');
const formContainer = document.querySelector('.formContainer');
const objectItemsSelection = document.querySelector('.objectItemsSelection');
const doneScreen = document.querySelector('.done');
const details = document.querySelector('.details');
const detailsNumber = document.querySelector('.details-block-input.number');
const detailsYear = document.querySelector('.details-block-input.year');
const contactLinkMobile = document.querySelector('.contact-mobile');
const contactLinkEmail = document.querySelector('.contact-email');
const stepper = document.querySelector('.steps');
const stepperText = stepper.querySelector('span');
const logo = document.querySelector('.logo');
const doneInner = document.querySelector('.doneInner');
const inactiveMessage = document.querySelector('.inactiveMessage');

let selectedObjects = [];
let _form;
let negPage = false;

const resetStepperStepText = nCurr => {
  const txt = stepperText.innerText;
  const nTotal = txt.split(' ').reverse()[0];
  stepperText.innerText = `Stap ${Math.min(nCurr, nTotal)} van ${nTotal}`;
};

const resetObjectDetailsNode = pageN => {
  if (selectedObjects[pageN]) {
    setTimeout(() => {
      if (!formContainer.querySelector('.step-object-details')) {
        formContainer
          .querySelector('.formio-form>div')
          .insertBefore(
            selectedObjects[pageN].formNode,
            formContainer.querySelector('.wizard-page')
          );
      }
      showNode(selectedObjects[pageN].formNode);
    }, 0);
  }
};

stepper.querySelector('button').addEventListener('click', () => {
  negPage = _form.page === 0;
  if (_form.page > 0) {
    resetStepperStepText(_form.page);
    _form.prevPage();
    resetObjectDetailsNode(_form.page - 1);
  }
});

nextStepButton.addEventListener('click', () => {
  if (_form.page === _form.pages.length - 1) {
    _form.submit();
  }
  resetStepperStepText(_form.page + 2);
  _form.nextPage();
  resetObjectDetailsNode(_form.page + 1);
});

let initData = {
  token: new URLSearchParams(window.location.search).get('token'),
  year: undefined,
  aanslagnumber: undefined,
  koppelAppObjectionIntegration: undefined,
  koppelAppCaseIntegration: undefined,
  koppelAppCyclomediaIntegration: undefined,
  objects: [],
  formConfig: undefined,
  noObjectsText: '',
};

const currencyIntl = new Intl.NumberFormat('nl-NL', {
  style: 'currency',
  currency: 'EUR',
});

const showNode = (node, flex = false) => {
  node.style.display = flex ? 'flex' : 'block';
};

const hideNode = node => {
  node.style.display = 'none';
};

const templates = {
  objectItem: templatesEl.children[0],
  objectForm: templatesEl.children[1],
};

const makeToggleObjectSelectionListener = (object, node) => () => {
  if (selectedObjects.includes(object)) {
    node.removeAttribute('selected');
    node.parentNode.removeAttribute('aria-selected');
  } else {
    node.setAttribute('selected', 'true');
    node.parentNode.setAttribute('aria-selected', 'true');
  }

  // Making sure the order of selected objects is the same as in original list
  selectedObjects = initData.objects.filter(
    obj =>
      (selectedObjects.includes(obj) && obj !== object) ||
      (!selectedObjects.includes(obj) && obj === object)
  );

  startButton.disabled = !selectedObjects.length;
};

const getPayload = data => {
  return selectedObjects.map((obj, ix) => ({
    ...obj.original,
    ...Object.keys(data)
      .filter(key => key.includes('obj_' + ix) || !key.includes('obj_'))
      .reduce((acc, key) => {
        acc[key.replace('obj_' + ix + '_', '')] = data[key];
        return acc;
      }, {}),
    meta: { name: 'bezwaar' },
  }));
};

const styleFactory = () => {
  const style = document.createElement('style');

  return [
    (selector, propertyName, value) => {
      if (value) {
        style.innerText += `${selector}:not(#\\9) { ${propertyName}: ${value}; } `;
      }
    },
    () => {
      document.body.appendChild(style);
    },
  ];
};

const appendStyleFromConfig = interfaceConfig => {
  const [addRules, appendStylesheet] = styleFactory();
  addRules('.details', 'background-color', interfaceConfig.theme_color_header);
  addRules('.details-block', 'color', interfaceConfig.theme_color_header_font);
  addRules(
    '.object-horizontal',
    'background-color',
    interfaceConfig.theme_color_object_deselected
  );
  addRules(
    '.object-details-postal-code',
    'color',
    interfaceConfig.theme_color_object_deselected_font_postcode
  );
  addRules(
    '.object-details-street',
    'color',
    interfaceConfig.theme_color_object_deselected_font_street
  );
  addRules(
    '.object-details-woz-worth',
    'color',
    interfaceConfig.theme_color_object_deselected_font_worth
  );
  addRules(
    '.object-horizontal[selected]',
    'background-color',
    interfaceConfig.theme_color_object_selected
  );
  addRules(
    '.object-horizontal[selected]',
    'border-color',
    interfaceConfig.theme_color_object_selected
  );
  addRules(
    '.object-horizontal[selected]>.object-details>.object-details-postal-code',
    'color',
    interfaceConfig.theme_color_object_selected_font_postcode
  );
  addRules(
    '.object-horizontal[selected]>.object-details>.object-details-street',
    'color',
    interfaceConfig.theme_color_object_selected_font_street
  );
  addRules(
    '.object-horizontal[selected]>.object-details>.object-details-woz-worth',
    'color',
    interfaceConfig.theme_color_object_selected_font_worth
  );
  addRules(
    '.object-selected-icon-svg',
    'fill',
    interfaceConfig.theme_color_object_selected
  );
  addRules(
    '.button',
    'background-color',
    interfaceConfig.theme_color_primary_button
  );
  addRules(
    '.button:hover',
    'background-color',
    interfaceConfig.theme_color_primary_button_hover
  );
  addRules('.button', 'color', interfaceConfig.theme_color_primary_button_font);
  addRules(
    '.button:hover',
    'color',
    interfaceConfig.theme_color_primary_button_font_hover
  );
  addRules('.alert.alert-danger', 'display', 'none');
  addRules('.alert.alert-success', 'display', 'none');
  addRules('.formio-wizard-nav-container', 'display', 'none');
  addRules('nav', 'display', 'none');

  appendStylesheet();
};

const showError = () => {
  loader.style.animation = 'none';

  alert(
    'Er is iets misgegaan. Wij verzoeken u om telefonisch contact met ons op te nemen zodat wij u kunnen helpen.'
  );
};

const parse = data => {
  initData.aanslagnumber = data[0].WOZ_AANSLAG;
  initData.year = data[0].WOZ_AANSLAGJAAR;
  initData.name = data[0].SUBJECT_NAAM;
  initData.noObjectsText =
    data[0].no_objects_text ||
    'Er zijn woningen waartegen bezwaar kan worden ingediend.';
  initData.objects = data
    .map(obj => {
      const houseNumber =
        obj.WOZ_HUISNUMMER +
        obj.WOZ_HUISNUMMERLETTER +
        (obj.WOZ_HUISNUMMERTOEVOEGING
          ? '-' + obj.WOZ_HUISNUMMERTOEVOEGING
          : '');
      return {
        original: obj,
        id: obj.id,
        worth:
          'WOZ-waarde: ' +
          currencyIntl.format(obj.WOZ_WAARDE).replace(',00', ''),
        postal: obj.WOZ_POSTCODE + ' ' + obj.WOZ_PLAATS,
        aanslagnumber: obj.WOZ_AANSLAG,
        houseNumber,
        postcode: obj.WOZ_POSTCODE,
        street: obj.WOZ_STRAAT + ' ' + houseNumber,
        objectionSubmitted: Boolean(obj.objection_ts),
      };
    })
    .filter(obj => !obj.objectionSubmitted);
};

const handleV1Res = (res, forwards, goToInactive) => {
  const config = res.result.instance.rows[0].instance.interface_config;
  appendStyleFromConfig(config);

  logo.innerHTML = config.header_logo;
  inactiveMessage.innerHTML = config.inactive_message;
  config.done_message && (doneInner.innerHTML = config.done_message);
  initData.introductionText = [
    config.introduction_text_single,
    config.introduction_text_multiple,
  ];
  contactLinkMobile.href = 'tel:' + config.contact_number;
  contactLinkEmail.href = 'mailto:' + config.contact_email;
  initData.koppelAppObjectionIntegration = config.endpoint_get;
  initData.koppelAppCaseIntegration = config.endpoint_post;
  initData.koppelAppCyclomediaIntegration = config.endpoint_cyclomedia;
  initData.formConfig = JSON.parse(config.form_json);

  return fetch(initData.koppelAppObjectionIntegration, {
    method: 'POST',
    body: initData.token,
    headers: { 'Content-Type': 'application/json' },
  })
    .then(rz => rz.json())
    .then(res => {
      if (config.is_active) {
        parse(res);
        forwards();
      } else {
        goToInactive();
      }
    });
  // .catch(() => parse(window.koppelMockRes))
};

const prepObjectSelection = () => {
  if (initData.objects.length === 0) {
    selectionStep.children[0].innerText = initData.noObjectsText;
    hideNode(startButton);
  } else if (initData.objects.length === 1) {
    initData.objects.forEach(object => {
      const rootNode = templates.objectItem.cloneNode(true);
      const node = rootNode.children[0];
      node.children[2].children[0].innerText = object.street;
      node.children[2].children[1].innerText = object.postal;
      node.children[2].children[2].innerText = object.worth;
      node.setAttribute('selected', 'true');
      rootNode.setAttribute('aria-selected', 'true');
      selectedObjects.push(object);
      objectItemsSelection.appendChild(rootNode);
      object.itemNode = node;
    });

    selectionStep.children[0].innerHTML = initData.introductionText[0];
  } else {
    startButton.disabled = true;
    initData.objects.forEach(object => {
      const rootNode = templates.objectItem.cloneNode(true);
      const node = rootNode.children[0];
      node.children[2].children[0].innerText = object.street;
      node.children[2].children[1].innerText = object.postal;
      node.children[2].children[2].innerText = object.worth;

      node.addEventListener(
        'click',
        makeToggleObjectSelectionListener(object, node)
      );
      objectItemsSelection.appendChild(rootNode);
      object.itemNode = node;
    });

    selectionStep.children[0].innerHTML = initData.introductionText[1];
  }

  initData.objects.forEach(object => {
    const node = templates.objectForm.cloneNode(true);
    node.querySelector('.object-details-street').innerText = object.street;
    node.querySelector('.object-details-postal-code').innerText = object.postal;
    node.querySelector('.object-details-woz-worth').innerText = object.worth;

    object.formNode = node;
  });

  initData.objects.forEach(object => {
    fetch(initData.koppelAppCyclomediaIntegration, {
      method: 'POST',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify({
        postcode: object.postcode,
        house_number: object.houseNumber,
      }),
    })
      .then(rz => rz.blob())
      .then(blob => {
        const itemImg = object.itemNode.querySelector('.object-image>img');
        const formImg = object.formNode.querySelector('.object-image>img');
        const url = URL.createObjectURL(blob);

        itemImg.src = url;
        formImg.src = url;
      })
      .catch(console.log)
      .then(() => {
        setTimeout(() => {
          document.querySelectorAll('.loader-sm-container').forEach(el => {
            el.remove();
          });
        }, 200);
      });
  });

  detailsYear.innerText = initData.year;
  detailsNumber.innerText = initData.aanslagnumber;
};

const steps = [
  {
    name: 'initialize',
    onStart: (forwards, __, goToInactive) => {
      fetch('/api/v1/sysin/interface/get_by_module_name/koppelapp_objections')
        .then(rz => rz.json())
        .then(res => handleV1Res(res, forwards, goToInactive))
        // .catch(() => handleV1Res(window.mockRes, forwards, goToInactive))
        .then(prepObjectSelection)
        .catch(showError);
    },
  },
  {
    name: 'selection',
    onStart: forwards => {
      showNode(selectionStep);
      showNode(startButton);
      hideNode(nextStepButton);
      hideNode(loader);
      hideNode(formContainer);
      hideNode(stepper);
      document.querySelector('footer').style.position = 'static';
      details.style.display = 'flex';
      startButton.addEventListener('click', forwards);
    },
  },
  {
    name: 'form',
    onStart: (forwards, back) => {
      const handleBackAtZero = () => _form.page === 0 && negPage && back();
      formContainer.innerHTML = '';
      showNode(formContainer);
      hideNode(selectionStep);
      showNode(stepper, true);
      hideNode(startButton);
      showNode(nextStepButton);
      stepper
        .querySelector('button')
        .addEventListener('click', handleBackAtZero);
      stepperText.innerText = `Stap 1 van ${
        selectedObjects.length + initData.formConfig.components.length - 1
      }`;
      resetObjectDetailsNode(0);

      const objStepTpl = initData.formConfig.components.slice(0, 1)[0];
      const compRest = initData.formConfig.components.slice(1);
      const fc = {
        ...initData.formConfig,
        components: [
          ...selectedObjects.map((obj, ix) => ({
            ...objStepTpl,
            key: objStepTpl.key + ix,
            components: objStepTpl.components.map(co => ({
              ...co,
              key: `obj_${ix}_${co.key}`,
              conditional: co.conditional
                ? {
                    ...co.conditional,
                    when: `obj_${ix}_${co.conditional.when}`,
                  }
                : null,
            })),
          })),
          ...compRest,
        ],
      };

      Formio.createForm(formContainer, fc, {
        language: 'nl',
        i18n: {
          nl: {
            'Add Another': 'Een andere toevoegen',
            'File Name': 'Bestandsnaam',
            Size: 'Grootte',
            or: 'of',
            'Drop files to attach,': 'Sleep bestanden hierheen,',
            required: 'Veld is verplicht',
            browse: 'Voeg bestanden toe',
            Month: 'Maand',
            Day: 'Dag',
            Year: 'Jaar',
            January: 'Januari',
            February: 'Februari',
            March: 'Maart',
            April: 'April',
            May: 'Mei',
            June: 'Juni',
            July: 'Juli',
            August: 'Augustus',
            October: 'Oktober',
          },
        },
        hooks: {
          beforeCancel: () => {
            back();
          },
        },
      }).then(form => {
        _form = form;
        form.on('submit', submission => {
          Promise.all(
            getPayload(submission.data).map(pd =>
              fetch(initData.koppelAppCaseIntegration, {
                method: 'POST',
                body: JSON.stringify(pd),
                headers: { 'Content-Type': 'application/json' },
              })
            )
          )
            .then(() => {
              forwards();
            })
            .catch(showError);
        });
      });

      return () =>
        stepper
          .querySelector('button')
          .removeEventListener('click', handleBackAtZero);
    },
  },
  {
    name: 'done',
    onStart: () => {
      showNode(doneScreen);
      hideNode(formContainer);
      hideNode(nextStepButton);
      hideNode(stepper);
    },
  },
  {
    name: 'inactive',
    onStart: () => {
      hideNode(loader);
      hideNode(selectionStep);
      showNode(inactiveMessage);
    },
  },
];

const initSteps = () => {
  let activeStep = -1;
  let remover;

  const goToInactive = () => {
    steps.find(st => st.name === 'inactive').onStart(goForwards, goBack);
  };

  const pushStep = forwards => () => {
    activeStep = activeStep + (forwards ? 1 : -1);
    remover && remover();
    remover = steps[activeStep].onStart(goForwards, goBack, goToInactive);
  };

  const goForwards = pushStep(true);
  const goBack = pushStep(false);

  window.forwards = goForwards;
  window.back = goBack;
  window.inactive = goToInactive;

  goForwards();
};

initSteps();
