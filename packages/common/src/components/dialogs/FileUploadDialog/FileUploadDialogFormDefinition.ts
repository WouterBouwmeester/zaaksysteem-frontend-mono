// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { UPLOAD } from '@zaaksysteem/common/src/components/form/constants/fieldTypes';
export const getFormDefinition = (multiValue: boolean) => [
  {
    name: 'files',
    type: UPLOAD,
    value: [],
    required: true,
    label: multiValue
      ? 'FileLinkerDialog:label.multiple'
      : 'FileLinkerDialog:label.single',
    placeholder: 'FileLinkerDialog:placeholder',
    format: 'file' as 'file',
    multiValue,
  },
];
