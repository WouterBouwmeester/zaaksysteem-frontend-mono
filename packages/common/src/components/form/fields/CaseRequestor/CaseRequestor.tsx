// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React, { useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';
import classnames from 'classnames';
import { ValueType } from '@mintlab/ui/App/Zaaksysteem/Select/types/ValueType';
import useServerErrorDialog from '@zaaksysteem/common/src/hooks/useServerErrorDialog';
import { FormFieldComponentType } from '../../types/Form2.types';
import { CaseRequestorConfigType } from './CaseRequestor.types';
import { useStyles } from './CaseRequestor.styles';
import { fetchRequestors } from './CaseRequestor.library';

/* eslint complexity: [2, 7] */
const CaseRequestor: FormFieldComponentType<
  ValueType<string>,
  CaseRequestorConfigType
> = ({ config, setFieldValue, name, value, setFieldTouched }) => {
  const [{ busy, error }, setFetchState] = useState({
    busy: false,
    error: false,
  });
  const [ServerErrorDialog, openServerErrorDialog] = useServerErrorDialog();
  const classes = useStyles();
  const [t] = useTranslation('common');

  const fetchData = async () => {
    const requestors = await fetchRequestors(config, openServerErrorDialog);

    if (requestors && requestors.length) {
      setFieldValue(name, requestors[0]);
      setFieldTouched(name);
      setFetchState({
        busy: false,
        error: false,
      });
    } else {
      setFetchState({
        busy: false,
        error: true,
      });
    }
  };

  useEffect(() => {
    setFetchState({
      busy: true,
      error: false,
    });
    fetchData();
  }, [config]);

  const errorMessage =
    config && config.errorMessage
      ? config.errorMessage
      : t('caseRequestor.error');

  return (
    <div className={classnames(classes.wrapper, error && classes.error)}>
      {busy && t('caseRequestor.loading')}
      {error && errorMessage}
      {value?.label}
      {ServerErrorDialog}
    </div>
  );
};

export default CaseRequestor;
