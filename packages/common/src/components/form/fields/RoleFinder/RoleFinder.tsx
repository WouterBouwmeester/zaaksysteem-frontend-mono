// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import { DataProvider } from '@mintlab/ui/App/Abstract/DataProvider';
//@ts-ignore
import { isPopulatedArray } from '@mintlab/kitchen-sink/source';
import FlatValueSelect from '@zaaksysteem/common/src/components/form/fields/FlatValueSelect';
import useServerErrorDialog from '@zaaksysteem/common/src/hooks/useServerErrorDialog';
import { ValueType } from '@mintlab/ui/App/Zaaksysteem/Select/types/ValueType';
import { FormFieldComponentType } from '../../types/Form2.types';
import { fetchRoleChoices } from './RoleFinder.library';

const RoleFinder: FormFieldComponentType<
  ValueType<string>,
  { parentRoleUuid: string }
> = ({ value, name, config, ...restProps }) => {
  const [ServerErrorDialog, openServerErrorDialog] = useServerErrorDialog();
  const parentRoleUuid = config?.parentRoleUuid;

  return (
    <React.Fragment>
      <DataProvider
        autoProvide={Boolean(parentRoleUuid)}
        provider={fetchRoleChoices(openServerErrorDialog)}
        providerArguments={[parentRoleUuid]}
      >
        {({ data, busy, provide }) => {
          const normalizedChoices = data || [];
          return (
            <FlatValueSelect
              {...restProps}
              name={name}
              choices={normalizedChoices}
              value={value}
              isClearable={true}
              loading={busy}
              getChoices={provide}
              isDisabled={!isPopulatedArray(normalizedChoices)}
            />
          );
        }}
      </DataProvider>
      {ServerErrorDialog}
    </React.Fragment>
  );
};

export default RoleFinder;
