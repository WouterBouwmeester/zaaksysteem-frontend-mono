// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { request } from '@zaaksysteem/common/src/library/request/request';
import { APICaseManagement } from '@zaaksysteem/generated';
import { OpenServerErrorDialogType } from '@zaaksysteem/common/src/hooks/useServerErrorDialog';
import { buildUrl } from '@mintlab/kitchen-sink/source';
import {
  GetSummaryType,
  SortRolesType,
  MapRolesType,
} from './RolesFinder.types';

const getSummary: GetSummaryType = role => role.meta.summary.toLowerCase();

const sortRoles: SortRolesType = roles => {
  const sortedRoles = roles.sort((roleA, roleB) =>
    getSummary(roleA) < getSummary(roleB) ? -1 : 1
  );

  const systemRoles = sortedRoles.filter(role =>
    Boolean(role.attributes?.description)
  );

  const departmentRoles = sortedRoles.filter(
    role => !role.attributes?.description
  );

  return [...departmentRoles, ...systemRoles];
};

const mapRoles: MapRolesType = roles =>
  roles.map(role => ({
    label: role.attributes?.name || '',
    value: role.id,
  }));

export const fetchRoleChoices =
  (onError: OpenServerErrorDialogType) => async (roleUuid: string) => {
    if (!roleUuid) return [];

    const body = await request<APICaseManagement.GetRolesResponseBody>(
      'GET',
      buildUrl<APICaseManagement.GetRolesRequestParams>(
        '/api/v2/cm/authorization/get_roles',
        {
          filter: {
            'relationships.parent.id': roleUuid,
          },
        }
      )
    ).catch(onError);

    const roles = body?.data || [];
    const sortedRoles = sortRoles(roles);
    const mappedRoles = mapRoles(sortedRoles);

    return mappedRoles;
  };
