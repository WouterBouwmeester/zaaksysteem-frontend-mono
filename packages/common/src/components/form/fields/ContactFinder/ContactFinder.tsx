// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React, { useState, useEffect } from 'react';
import MultilineOption from '@mintlab/ui/App/Zaaksysteem/Select/Option/MultilineOption';
import { ValueType } from '@mintlab/ui/App/Zaaksysteem/Select/types/ValueType';
import { Select } from '@mintlab/ui/App/Zaaksysteem/Select';
import useServerErrorDialog from '@zaaksysteem/common/src/hooks/useServerErrorDialog';
import { usePrevious } from '@zaaksysteem/common/src/hooks/usePrevious';
import { FormFieldComponentType } from '../../types/Form2.types';
import { fetchContactChoices } from './ContactFinder.library';

const ContactFinder: FormFieldComponentType<ValueType<string>, any> = ({
  multiValue,
  config,
  value,
  ...restProps
}) => {
  const [ServerErrorDialog, openServerErrorDialog] = useServerErrorDialog();
  const { choices } = restProps;
  const [internalChoices, setInternalChoices] = useState([] as any);
  const [loading, setLoading] = useState(false);
  const prev = usePrevious(choices);

  const fetchContactChoicesFunction = fetchContactChoices(
    config,
    openServerErrorDialog
  );
  const getChoices = async (input: string) => {
    setLoading(true);
    const result = (await fetchContactChoicesFunction(input)) || [];
    setInternalChoices(result);
    setLoading(false);
    return result;
  };

  useEffect(() => {
    if (choices && prev !== choices) {
      setInternalChoices(choices);
    }
  }, [choices]);

  return (
    <React.Fragment>
      <Select
        {...restProps}
        value={value}
        choices={internalChoices}
        isClearable={true}
        loading={loading}
        isMulti={multiValue}
        getChoices={getChoices}
        components={{
          ...restProps.components,
          Option: MultilineOption,
        }}
        filterOption={() => true}
        multiValueLabelIcon={config?.multiValueLabelIcon}
      />

      {ServerErrorDialog}
    </React.Fragment>
  );
};

export default ContactFinder;
