// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
import { request } from '@zaaksysteem/common/src/library/request/request';
import { OpenServerErrorDialogType } from '@zaaksysteem/common/src/hooks/useServerErrorDialog';
import { ValueType } from '@mintlab/ui/App/Zaaksysteem/Select/types/ValueType';

type ChoiceType = {
  type: string;
  id: any;
  attributes?: {
    name: string;
    code: number;
  };
};

export const fetchCountries =
  (onServerError: OpenServerErrorDialogType) =>
  async (): Promise<ValueType<string>[]> => {
    const body = await request<any>(
      'GET',
      '/api/v2/cm/contact/get_countries'
    ).catch(onServerError);

    let unknown: any;

    const countries = body
      ? body.data
          ?.map((choice: ChoiceType) => {
            return {
              label: choice?.attributes?.name,
              value: choice?.attributes?.code,
            };
          })
          .filter((choice: ValueType<any>) => {
            if (choice?.value === 0) unknown = choice;
            return choice?.value !== 0;
          })
          .sort((aChoice: ValueType<any>, bChoice: ValueType<any>) =>
            //@ts-ignore
            aChoice.label > bChoice.label ? 1 : -1
          )
      : [];

    if (!countries || !countries?.length) return [];
    return [...[unknown], ...countries];
  };
