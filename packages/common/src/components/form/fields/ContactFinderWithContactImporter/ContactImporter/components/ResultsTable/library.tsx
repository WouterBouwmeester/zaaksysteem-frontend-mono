// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
import React, { Fragment } from 'react';
import * as i18next from 'i18next';
import Tooltip from '@mintlab/ui/App/Material/Tooltip';
import Icon, { iconNames } from '@mintlab/ui/App/Material/Icon';
import { PersonTableRowType } from '../../types';

export const getWarnings = ({
  rowData,
  t,
}: {
  rowData: PersonTableRowType;
  t: i18next.TFunction;
}): React.ReactElement | null => {
  let warnings: string[] = [];
  if (rowData.isDeceased)
    warnings.push(t('ContactImporter:warnings.isDeceased'));
  if (rowData.isSecret) warnings.push(t('ContactImporter:warnings.isSecret'));
  if (rowData.hasCorrespondenceAddress)
    warnings.push(t('ContactImporter:warnings.hasCorrespondenceAddress'));
  if (rowData.isInResearch)
    warnings.push(t('ContactImporter:warnings.isInResearch'));

  return warnings.length ? (
    <div>
      <Tooltip
        title={
          <Fragment>
            {warnings.map((warning, index) => (
              <div key={index}>{warning}</div>
            ))}
          </Fragment>
        }
      >
        <Icon size="extraSmall">{iconNames.warning}</Icon>
      </Tooltip>
    </div>
  ) : null;
};
