// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { v4 } from 'uuid';
import { SubjectTypeType } from '@zaaksysteem/common/src/types/SubjectTypes';
import { isSearchableInterface } from '../library';
import { StoreShapeType, AnyRecordType } from '../types';

type GetInitialStateType = (params: {
  subjectTypes: SubjectTypeType[];
}) => StoreShapeType;

export const getInitialState: GetInitialStateType = ({ subjectTypes }) => ({
  loading: false,
  initializing: true,
  interfaces: [],
  capabilities: [],
  rows: null,
  allowedFormTypes: [],
  selectedFormType: 'company',
  selectedInterface: 'local',
  externalSearchAllowed: false,
  subjectTypes,
  importReference: null,
  importing: false,
  activeInterfaces: [],
});

/* eslint complexity: [2, 20] */
export const reducer = (state: StoreShapeType, action: any): StoreShapeType => {
  const { payload } = action;

  switch (action.type) {
    case 'setInitializing':
    case 'setLoading':
      return {
        ...state,
        loading: payload,
      };
    case 'setSessionData':
      return {
        ...state,
        capabilities: payload.logged_in_user.capabilities,
        activeInterfaces: payload.active_interfaces,
      };
    case 'setModulesConfig':
      return {
        ...state,
        interfaces: payload
          .map((interf: any) => ({
            uuid: interf.reference,
            id: interf.instance.id,
            name: interf.instance.interface_config.search_form_title,
            config: interf.instance.interface_config,
          }))
          .filter((interf: any) =>
            isSearchableInterface(interf, state.capabilities)
          )
          .sort((sortA: any, sortB: any) => (sortA.id > sortB.id ? 1 : -1)),
      };
    case 'initDefaults': {
      let allowedFormTypes: string[] = [];

      if (state.subjectTypes.includes('person')) {
        allowedFormTypes.push('person');
      }

      if (state.subjectTypes.includes('organization')) {
        allowedFormTypes.push('company');
      }

      return {
        ...state,
        allowedFormTypes,
        selectedFormType:
          allowedFormTypes[0] as StoreShapeType['selectedFormType'],
        externalSearchAllowed: isExternalSearchAllowed(
          state,
          allowedFormTypes[0]
        ),
        initializing: false,
      };
    }
    case 'setFormType': {
      return {
        ...state,
        selectedFormType: payload,
        externalSearchAllowed: isExternalSearchAllowed(state, payload),
        selectedInterface: 'local',
      };
    }
    case 'setSelectedInterface': {
      return {
        ...state,
        selectedInterface: payload,
      };
    }
    case 'setSearchResults':
      return {
        ...state,
        rows: payload.rows.map((thisRow: AnyRecordType) => ({
          ...thisRow,
          uuid: thisRow.reference || v4(),
        })),
      };
    case 'clearSearchResults':
      return {
        ...state,
        rows: [],
      };
    case 'setImportReference':
      return {
        ...state,
        importReference: payload,
      };
    case 'setImporting':
      return {
        ...state,
        importing: payload,
      };
    default:
      return state;
  }
};

const isExternalSearchAllowed = (state: any, type: any) => {
  if (type === 'person') {
    return (
      Boolean(state.interfaces.length) &&
      state.capabilities.includes('contact_search_extern') &&
      state.subjectTypes.includes('person')
    );
  } else if (type === 'company') {
    return state.activeInterfaces.includes('kvkapi');
  }
};
