// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
//@ts-ignore
import { LoadableKeyboardDatePicker } from '@mintlab/ui/App/Material/DatePicker';
import { ReadonlyValuesContainer } from '../library/ReadonlyValuesContainer';
import { FormFieldComponentType } from '../types/Form2.types';

const KeyboardDatePicker: FormFieldComponentType<string> = props => {
  const { onClose, setFieldValue, name, readOnly, value } = props;

  return readOnly ? (
    <ReadonlyValuesContainer value={value} />
  ) : (
    <LoadableKeyboardDatePicker
      {...props}
      onClose={() => {
        if (onClose) {
          onClose();
        } else {
          setFieldValue(name, null);
        }
      }}
    />
  );
};

export default KeyboardDatePicker;
