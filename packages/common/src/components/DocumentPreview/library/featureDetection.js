// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

export const isPdfSupported = () => {
  const safari = /^((?!chrome|android).)*safari/i.test(navigator.userAgent);
  function hasAcrobatInstalled() {
    function getActiveXObject(name) {
      try {
        return new window.ActiveXObject(name);
      } catch (error) {
        // ignore errors
      }
    }

    return getActiveXObject('AcroPDF.PDF') || getActiveXObject('PDF.PdfCtrl');
  }

  return (
    !safari && (navigator.mimeTypes['application/pdf'] || hasAcrobatInstalled())
  );
};
