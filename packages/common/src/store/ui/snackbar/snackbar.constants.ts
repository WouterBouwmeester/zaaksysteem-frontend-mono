// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

export const SHOW_SNACKBAR = 'SHOW_SNACKBAR';
export const HIDE_SNACKBAR = 'HIDE_SNACKBAR';
