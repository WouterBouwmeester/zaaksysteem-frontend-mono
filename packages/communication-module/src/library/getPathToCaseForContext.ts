// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { CommunicationContextContextType } from '../types/Context.types';

const getPathToCaseForContext = (
  context: CommunicationContextContextType,
  caseNumber: number
): string => {
  if (context === 'pip') {
    return `/pip/zaak/${caseNumber}`;
  }

  return `/intern/zaak/${caseNumber}`;
};

export default getPathToCaseForContext;
