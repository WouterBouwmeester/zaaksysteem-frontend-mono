// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { createAjaxAction } from '@zaaksysteem/common/src/library/redux/ajax/createAjaxAction';
import { APICommunication } from '@zaaksysteem/generated';
import { IMPORT_MESSAGE } from './communication.importMessage.constants';

const importMessageAction = createAjaxAction(IMPORT_MESSAGE);

export type ImportMessagePayloadType = Pick<
  APICommunication.ImportEmailMessageRequestBody,
  'file_uuid' | 'case_uuid'
>;

export const importMessage = (payload: ImportMessagePayloadType) => {
  const { file_uuid, case_uuid } = payload;

  return importMessageAction<
    ImportMessagePayloadType,
    APICommunication.ImportEmailMessageRequestBody
  >({
    url: '/api/v2/communication/import_email_message',
    method: 'POST',
    data: {
      case_uuid,
      file_uuid,
    },
    payload,
  });
};
