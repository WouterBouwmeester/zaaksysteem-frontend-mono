// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { CommunicationRootStateType } from '../communication.reducer';
import { threadNotRelatedOrRelatedCaseNotResolvedSelector } from './threadNotRelatedOrRelatedCaseNotResolvedSelector';

export const canAddSourceFileToCaseSelector = (
  state: CommunicationRootStateType
) => {
  const {
    communication: {
      context: {
        capabilities: { canAddSourceFileToCase },
      },
    },
  } = state;

  return (
    threadNotRelatedOrRelatedCaseNotResolvedSelector(state) &&
    canAddSourceFileToCase
  );
};
