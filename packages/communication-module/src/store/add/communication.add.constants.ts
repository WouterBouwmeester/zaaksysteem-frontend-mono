// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { createAjaxConstants } from '@zaaksysteem/common/src/library/redux/ajax/createAjaxConstants';
export const SAVE_COMMUNICATION = createAjaxConstants('SAVE_COMMUNICATION');
export const SET_SAVE_COMMUNICATION_PENDING = 'SET_SAVE_COMMUNICATION_PENDING';
