// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React, { useMemo, useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { NestedFormValue } from '@zaaksysteem/common/src/components/form/types/formDefinition.types';
import mapValuesToFormDefinition from '@zaaksysteem/common/src/components/form/library/mapValuesToFormDefinition';
import {
  transferDataAsConfig,
  Rule,
} from '@zaaksysteem/common/src/components/form/rules';
import GenericMessageForm from '../GenericMessageForm/GenericMessageForm';
import { SaveMessageFormValuesType } from '../../../types/Message.types';
import { GenericMessageFormPropsType } from '../GenericMessageForm/GenericMessageForm.types';
import { fetchEmailIntegrations } from '../../../library/requests';
import { getEmailTemplateData } from '../../../library/getEmailTemplateData';
import { EmailTemplateDataType } from '../../../types/EmailIntegration.types';
import { getEmailFormDefinition } from './emailExternalMessageForm.formDefinition';

export type EmailExternalMessageFormProps<Values = SaveMessageFormValuesType> =
  Pick<
    GenericMessageFormPropsType<Values>,
    'save' | 'busy' | 'cancel' | 'top' | 'enablePreview'
  > & {
    caseUuid?: string;
    htmlEmailTemplateName?: string;
    contactUuid?: string;
    initialValues?: Partial<SaveMessageFormValuesType>;
    selectedRecipientType?: string;
  };

const flattenEmail = (item: NestedFormValue): string => {
  if (item.label && item.label !== item.value) {
    return `${item.label} <${item.value}>`;
  }

  return item.value.toString();
};

const mapPreviewValues = (
  values: SaveMessageFormValuesType
): Omit<SaveMessageFormValuesType, 'to' | 'cc' | 'bcc'> & {
  to: string[];
  cc: string[];
  bcc: string[];
} => {
  const mappedValues = {
    ...values,
    to: values.to.map(flattenEmail),
    cc: values.cc.map(flattenEmail),
    bcc: values.bcc.map(flattenEmail),
  };

  return mappedValues;
};

export const EmailExternalMessageForm: React.ComponentType<
  EmailExternalMessageFormProps
> = props => {
  const {
    caseUuid,
    contactUuid,
    initialValues,
    selectedRecipientType,
    htmlEmailTemplateName,
  } = props;
  const [t] = useTranslation('communication');
  const [emailTemplateData, setEmailTemplateData] = useState<
    EmailTemplateDataType | undefined
  >(undefined);

  useEffect(() => {
    async function fetchEmailTemplateSettings() {
      const emailIntegrations = await fetchEmailIntegrations();

      setEmailTemplateData(
        getEmailTemplateData(emailIntegrations, htmlEmailTemplateName)
      );
    }

    fetchEmailTemplateSettings();
  }, []);

  const htmlEmailTemplateDescription = htmlEmailTemplateName
    ? t('addFields.htmlEmailTemplateDescription', {
        htmlEmailTemplateName,
      })
    : undefined;
  const formDefinition = useMemo(() => {
    return getEmailFormDefinition({
      caseUuid,
      contactUuid,
      selectedRecipientType,
      htmlEmailTemplateDescription,
    });
  }, [caseUuid, contactUuid, selectedRecipientType]);
  const formDefinitionWithValues = useMemo(() => {
    return initialValues
      ? mapValuesToFormDefinition<SaveMessageFormValuesType>(
          initialValues,
          formDefinition
        )
      : formDefinition;
  }, []);

  return (
    <GenericMessageForm
      {...props}
      emailTemplateData={emailTemplateData}
      mapPreviewValues={mapPreviewValues}
      primaryButtonLabelKey={t('forms.send')}
      formName="email-form"
      formDefinition={formDefinitionWithValues}
      rules={[
        new Rule()
          .when(() => true)
          .then(transferDataAsConfig('case_uuid', 'to', 'caseUuid')),
      ]}
    />
  );
};
