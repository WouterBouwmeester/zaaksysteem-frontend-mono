// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { makeStyles } from '@material-ui/core';

export const useThreadPlaceholderStyle = makeStyles(
  ({ mintlab: { greyscale } }: any) => ({
    placeholder: {
      display: 'flex',
      flexDirection: 'column',
      justifyContent: 'center',
      height: '100%',
      color: greyscale.offBlack,
    },
  })
);
