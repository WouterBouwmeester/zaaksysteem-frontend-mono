// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

const inquirer = require('inquirer');
const readSchemaFile = require('./library/readSchemaFile');
const generateApiTypes = require('./generation/generateApiTypes');

process.on('unhandledRejection', error => {
  throw error;
});

async function promptForSchema() {
  const schemas = await readSchemaFile();
  const { domainsToGenerate } = await inquirer.prompt({
    type: 'checkbox',
    name: 'domainsToGenerate',
    validate: input => Boolean(input.length),
    message: 'Which domain do you wish to regenerate?',
    choices: ['All domains', ...Object.keys(schemas.domains)],
  });

  if (domainsToGenerate.includes('All domains')) {
    return schemas.domains;
  }

  return domainsToGenerate.reduce(
    (acc, name) => ({
      ...acc,
      [name]: schemas.domains[name],
    }),
    {}
  );
}

async function promptForEnvironmentUrl() {
  const { environment } = await inquirer.prompt({
    type: 'list',
    name: 'environment',
    message: 'Which environment do you wish to use?',
    choices: ['development', 'hotfix', 'other'],
  });

  if (environment === 'other') {
    const { customEnvironment } = await inquirer.prompt({
      type: 'input',
      name: 'customEnvironment',
      message: 'Please enter the full domain name',
    });

    return customEnvironment;
  }

  return `https://${environment}.zaaksysteem.nl`;
}

async function start() {
  const schemas = await promptForSchema();
  const environmentUrl = await promptForEnvironmentUrl();

  await generateApiTypes(environmentUrl, schemas);
}

start();
