// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
// eslint-disable-next-line import/no-unresolved
import toJson from 'enzyme-to-json';
// eslint-disable-next-line import/no-unresolved
import { shallow } from 'enzyme';
import { Fab } from './Fab';

/**
 * @test {Fab}
 */
describe('The `Fab` component', () => {
  test('renders correctly', () => {
    const component = shallow(
      <Fab
        aria-label={'ariaLabel'}
        color="primary"
        action={() => {}}
        scope={'fab'}
      >
        add
      </Fab>
    );

    expect(toJson(component)).toMatchSnapshot();
  });
});
