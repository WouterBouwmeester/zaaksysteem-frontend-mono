// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { default as Pagination } from './Pagination';
export * from './Pagination';
export default Pagination;
