// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { default as MaterialUiThemeProvider } from './MaterialUiThemeProvider';
export * from './MaterialUiThemeProvider';
export default MaterialUiThemeProvider;
