// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { React, stories, select } from '../../story';
import Switch from './Switch';

const stores = {
  Default: {
    checked: false,
  },
};

stories(
  module,
  __dirname,
  {
    Default({ store, checked }) {
      const onChange = ({ target }) => {
        store.set({
          checked: target.checked,
        });
      };

      return (
        <Switch
          variant={select('variant', ['regular', 'iOS'])}
          checked={checked}
          onChange={onChange}
        />
      );
    },
  },
  stores
);
