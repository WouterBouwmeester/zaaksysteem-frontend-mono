// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import Typography from '@material-ui/core/Typography';

/**
 * @see /npm-mintlab-ui/storybook/?selectedKind=Material/Typography&selectedStory=H3
 *
 * @param {Object} props
 * @param {*} props.children
 * @param {Object} [props.classes]
 * @return {ReactElement}
 */
export const H3 = ({ children, classes }) => (
  <Typography variant="h3" classes={classes}>
    {children}
  </Typography>
);
