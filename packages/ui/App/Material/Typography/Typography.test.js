// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import Typography from '.';
import * as theElementsOfStyle from '.';

/**
 * @test {Typography}
 */
describe('The `Typography` component', () => {
  test('has no `default` export', () => {
    const actual = Typography;
    const asserted = undefined;

    expect(actual).toBe(asserted);
  });

  describe('`has a named `H1` export`', () => {
    test('that is a function', () => {
      const actual = typeof theElementsOfStyle.H1;
      const asserted = 'function';

      expect(actual).toEqual(asserted);
    });
  });

  // ZS-TODO: add more tests
});
