// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import LazyLoader from '@mintlab/ui/App/Abstract/LazyLoader';

const LoadableDatePicker = props => (
  <LazyLoader
    promise={() =>
      import(
        // https://webpack.js.org/api/module-methods/#import
        /* webpackChunkName: "ui.datepicker" */
        './DatePicker'
      )
    }
    {...props}
  />
);

const LoadableKeyboardDatePicker = props => (
  <LazyLoader
    promise={() =>
      import(
        // https://webpack.js.org/api/module-methods/#import
        /* webpackChunkName: "ui.keyboarddatepicker" */
        './KeyboardDatePicker'
      )
    }
    {...props}
  />
);

export default LoadableDatePicker;
export { LoadableKeyboardDatePicker };
