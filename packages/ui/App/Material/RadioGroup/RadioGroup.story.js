// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { React, stories, boolean } from '../../story';
import RadioGroup from '.';

const stores = {
  Default: {
    value: '',
  },
};

stories(
  module,
  __dirname,
  {
    Default({ store }) {
      const { value } = store;
      const choices = [
        {
          label: 'Strawberry',
          value: 'strawberry',
        },
        {
          label: 'Chocolate',
          value: 'chocolate',
        },
        {
          label: 'Vanilla',
          value: 'vanilla',
          disabled: true,
        },
      ];

      const onChange = ({ target }) => {
        store.set({
          value: target.value,
        });
      };

      return (
        <RadioGroup
          choices={choices}
          disabled={boolean('Disabled', false)}
          name="foo"
          value={value}
          scope="story"
          onChange={onChange}
        />
      );
    },
  },
  stores
);
