// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

const normalizeItem = (item: string) => item.split(' ').join('-').toLowerCase();

const createScopeValue = (scope: string, ...rest: any[]) =>
  [scope, ...rest].filter(Boolean).map(normalizeItem).join(':');

const createScopeObject = (
  key: 'scope' | 'data-scope',
  scope: string,
  ...rest: any[]
) => {
  const value = createScopeValue(scope, ...rest);

  return scope ? { [key]: value } : {};
};

export const addScopeProp = (scope: string | undefined, ...rest: any[]) =>
  createScopeObject('scope', scope || '', ...rest) as { scope: string };

export const addScopeAttribute = (scope: string | undefined, ...rest: any[]) =>
  createScopeObject('data-scope', scope || '', ...rest) as {
    'data-scope': string;
  };
