// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
import React from 'react';
import { useProgressStyles } from './Progress.style';

type ProgressPropsType = {
  percentage: number;
};

const Progress: React.FunctionComponent<ProgressPropsType> = ({
  percentage,
}) => {
  const classes = useProgressStyles();

  return (
    <div className={classes.wrapper}>
      <div
        className={classes.progress}
        style={{
          width: `${percentage}%`,
        }}
      >
        &nbsp;
      </div>
    </div>
  );
};

export default Progress;
