// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { createElement } from 'react';
import { FormSelect } from '../Form/FormSelect';
import { CreatableSelect } from '../Form/CreatableSelect';
import { GenericSelect } from '../Generic/GenericSelect';

export const SelectStrategy = ({
  creatable = false,
  generic = false,
  ...rest
}) => {
  let component: any;

  if (creatable) {
    component = CreatableSelect;
  } else if (generic) {
    component = GenericSelect;
  } else {
    component = FormSelect;
  }

  return createElement(component, rest as any);
};

export default SelectStrategy;
