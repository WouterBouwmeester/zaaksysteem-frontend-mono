// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { default as ZsIcon } from './ZsIcon';
export * from './ZsIcon';
export default ZsIcon;
