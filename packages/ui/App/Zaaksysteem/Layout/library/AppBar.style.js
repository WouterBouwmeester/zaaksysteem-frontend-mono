// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

/**
 * Style Sheet for the AppBar component
 * @return {JSS}
 */
export const appBarStyleSheet = ({ breakpoints }) => ({
  appBar: {
    boxShadow: 'none',
  },
  content: {
    flexGrow: 1,
    padding: '0px 6px',
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    '&:children': {
      flexGrow: 0,
    },
  },
  searchContainer: {
    flexGrow: 1,
    alignItems: 'center',
    padding: '0 8px',
  },
  userName: {
    padding: '8px 8px 4px 8px',
  },
  toolbar: {
    [breakpoints.up('xs')]: {
      minHeight: '48px',
    },
  },
});
