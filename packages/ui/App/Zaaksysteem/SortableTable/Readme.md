# Sortable table

Sortable Table component, which takes arrays of rows and columns,
and generates a Virtualized table.

Basic support for:

- sorting on column content
- drag and drop row sorting
- custom cellrenderers
- responsive behaviour by showing/hiding columns
- dynamic or fixed row height
- checkboxes for selecting

## Types of sorting

- `sorting=none`: the table will not be sortable, clicking on the header columns will do nothing
- `sorting=column`: the table will be sortable by clickon the header columns. Default behaviour is internal sorting (see below)
- `sorting=dragdrop`: dragging and dropping rows is enabled as the only sorting method. The `onDragEnd` callback will be called after dropping a row, so the parent component can re-order the rows.

## External sorting

When `sortInternal=true`, column sorting will take place internally: clicking on a column header will re-order the `rows` according to the logic defined in the library function, and re-render the table.

When `sortInternal=false`, the component will do no sorting, but only apply the correct visual stylings to the header. The `onSort` callback is called, and it is up to the parent component to supply new, sorted, rows. This is useful in case the backend is handling the sorting.
