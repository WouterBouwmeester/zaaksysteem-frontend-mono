// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { React, stories, boolean, select, number } from '../../story';
import { Loader } from '.';

stories(module, __dirname, {
  Default() {
    const delay = number('Delay visible', 300);

    return (
      <Loader
        active={boolean('Active', true)}
        type={select(
          'Type',
          {
            circle: 'Circle',
          },
          'circle'
        )}
        delay={delay}
        scope="story"
      />
    );
  },
});
