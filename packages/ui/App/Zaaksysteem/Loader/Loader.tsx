// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React, { useState, useEffect } from 'react';
import { useTheme } from '@material-ui/styles';
import { Theme } from '@mintlab/ui/types/Theme';
import { addScopeProp } from '../../library/addScope';
import * as variants from './variants';

type LoaderPropsType = {
  active?: boolean;
  variant?: keyof typeof variants;
  delay?: number | string;
  className?: string;
  color?: any;
  scope?: string;
};

export const Loader: React.ComponentType<LoaderPropsType> = ({
  active = true,
  color,
  variant = 'circle',
  scope,
  delay = 0,
  className,
}) => {
  const [delayedActive, setDelayedActive] = useState(delay === 0);
  const visible = delayedActive && active;
  const theme = useTheme<Theme>();

  useEffect(() => {
    let timeoutId: number;

    if (delay > 0) {
      timeoutId = window.setTimeout(
        () => setDelayedActive(true),
        parseInt(delay.toString(), 10)
      );
    }

    return () => clearTimeout(timeoutId);
  });

  const LoaderComponent = variants[variant];

  if (visible && LoaderComponent) {
    return (
      <LoaderComponent
        color={color || theme.palette.common.black}
        className={className}
        {...addScopeProp(scope || '', 'loader')}
      />
    );
  }

  return null;
};
