// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

/**
 * Generates classnames through the withStyles HOC.
 * This gets injected as the `classes` prop into the Wywisyg component
 * @param {Object} theme
 * @return {Object}
 */
export const fileListStylesheet = () => ({
  wrapper: {
    width: '100%',
    '& > *': {
      marginBottom: 8,
    },
  },
});
