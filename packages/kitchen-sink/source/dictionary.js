// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

const { assign, create, defineProperties, keys } = Object;

/**
 * Create a clean Object
 * - without a prototype
 * - with immutable properties.
 *
 * @param {Object} interfaceObject
 * @param {Object} options
 * @return {Object}
 */
export function dictionary(interfaceObject, options = {}) {
  const { enumerable = true } = options;
  const reduceInterface = (descriptors, propertyName) =>
    assign(descriptors, {
      [propertyName]: {
        enumerable,
        value: interfaceObject[propertyName],
      },
    });

  return defineProperties(
    create(null),
    keys(interfaceObject).reduce(reduceInterface, create(null))
  );
}
