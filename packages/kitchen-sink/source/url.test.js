// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

const test = require('tape');
const {
  buildParamString,
  buildUrl,
  getSegment,
  getSegments,
  objectifyParams,
} = require('./url');

test('getSegments()', assert => {
  {
    const actual = getSegments('/foo/bar/baz');
    const expected = ['foo', 'bar', 'baz'];
    const message = 'returns the path segments of a given path component';

    assert.deepEqual(actual, expected, message);
  }
  {
    const actual = getSegments('/foo/bar?baz');
    const expected = ['foo', 'bar'];
    const message =
      'returns the path segments of a given path and query component';

    assert.deepEqual(actual, expected, message);
  }

  assert.end();
});

test('getSegment()', assert => {
  {
    const actual = getSegment('/foo/bar/baz');
    const expected = 'bar';
    const message = 'returns the second segment of a given path component';

    assert.equal(actual, expected, message);
  }
  {
    const actual = getSegment('/foo/bar?baz');
    const expected = 'bar';
    const message =
      'returns the second segment of a given path and query component';

    assert.equal(actual, expected, message);
  }

  assert.end();
});

test('buildParamString()', assert => {
  {
    const params = {
      one: 'first',
      two: 'second',
      three: 'third',
    };
    const actual = buildParamString(params);
    const expected = '?one=first&two=second&three=third';
    const message = 'returns the params of an object as a string with ?-prefix';

    assert.equal(actual, expected, message);
  }
  {
    const params = {};
    const actual = buildParamString(params);
    const expected = '';
    const message = 'returns an empty string for an empty object';

    assert.equal(actual, expected, message);
  }

  assert.end();
});

const url = '/api/v1/test';

test('buildUrl()', assert => {
  {
    const params = {
      one: 'first',
      two: 'second',
      three: 'third',
    };
    const actual = buildUrl(url, params);
    const expected = `${url}?one=first&two=second&three=third`;
    const message = 'returns the given path with params';

    assert.equal(actual, expected, message);
  }
  {
    const params = {
      one: 'f&irst',
      two: 's:econd',
      three: 't?hird',
    };
    const actual = buildUrl(url, params);
    const expected = `${url}?one=f%26irst&two=s%3Aecond&three=t%3Fhird`;
    const message = 'returns the given path with encoded params';

    assert.equal(actual, expected, message);
  }
  {
    const params = {
      one: 'first',
      two: '',
      three: null,
      four: 'fourth',
    };
    const actual = buildUrl(url, params);
    const expected = `${url}?one=first&four=fourth`;
    const message = 'returns the given path with params that have a value';

    assert.equal(actual, expected, message);
  }

  assert.end();
});

test('objectifyParams()', assert => {
  const paramString = 'foo=FOO&bar=BAR&quux=QUUX&test=%20';
  const actual = objectifyParams(paramString);
  const expected = {
    foo: 'FOO',
    bar: 'BAR',
    quux: 'QUUX',
    test: ' ',
  };
  const message =
    'transforms and decode a URL string of params to an object with key value pairs';

  assert.deepEqual(actual, expected, message);
  assert.end();
});
