const path = require('path');

module.exports = async ({ config }) => {
  config.node = {
    __dirname: true,
  };

  config.resolve = {
    ...(config.resolve || {}),
    modules: [
      path.resolve(__dirname, '..', 'node_modules'),
      path.resolve(__dirname, '..', '..', 'node_modules'),
    ],
    extensions: ['.js', '.ts', '.tsx', '.svg', '.css', '.png'],
    alias: {
      '@zaaksysteem': path.resolve(__dirname, '..', '..', 'packages'),
    },
  };

  config.module = {
    ...(config.module || {}),
    strictExportPresence: true,
    rules: [
      ...((config.module && config.module.rules) || []),
      {
        test: /\.(js|mjs|jsx|ts|tsx)$/,
        include: [
          path.resolve(__dirname, '..', '..', 'packages', 'common', 'src'),
          path.resolve(__dirname, '..', '..', 'packages', 'ui', 'App'),
          path.resolve(
            __dirname,
            '..',
            '..',
            'packages',
            'communication-module',
            'src'
          ),
          path.resolve(
            __dirname,
            '..',
            '..',
            'packages',
            'kitchen-sink',
            'source'
          ),
        ],
        loader: require.resolve('babel-loader'),
        options: {
          babelrc: false,
          configFile: false,
          cacheDirectory: true,
          cacheCompression: false,
          compact: true,
          presets: [
            [
              '@babel/preset-env',
              {
                useBuiltIns: 'entry',
                corejs: 3,
                exclude: ['transform-typeof-symbol'],
              },
            ],
            '@babel/preset-typescript',
            [
              '@babel/preset-react',
              {
                development: true,
                useBuiltIns: true,
              },
            ],
          ],
          plugins: [
            '@babel/plugin-proposal-optional-chaining',
            '@babel/plugin-proposal-nullish-coalescing-operator',
            [
              '@babel/plugin-proposal-class-properties',
              {
                loose: true,
              },
            ],
            [
              '@babel/plugin-transform-runtime',
              {
                regenerator: true,
              },
            ],
          ],
        },
      },
      {
        test: /\.woff(2)?(\?v=[0-9]\.[0-9]\.[0-9])?$/,
        use: [
          {
            loader: 'file-loader',
            options: {
              name: '[name].[ext]',
              mimetype: 'application/font-woff',
            },
          },
        ],
      },
      {
        test: /.svg$/,
        use: [
          {
            loader: 'file-loader',
            options: {
              name: '[name].[ext]',
              mimetype: 'image/svg+xml',
            },
          },
        ],
      },
      {
        test: /.png$/,
        use: [
          {
            loader: 'file-loader',
            options: {
              name: '[name].[ext]',
              mimetype: 'image/png',
            },
          },
        ],
      },
    ],
  };

  return config;
};
