# Redux modules

## 👣 Reducing the store's footprint

> Why initialize state that you will not use?

When your application grows, the store grows with it. Middleware gets added and is actively present in the background, even for parts of the application that remain untouched throughout the user session.
The concept behind Redux modules from `redux-dynamic-modules` is to split the store into small lazy loadable pieces. Added and removing parts of the store as the user clicks around in the application.

Every module contains:

- Reducers
- Middleware
- Initial actions

## Module entry point

Every module has a single entrypoint file identifiable by the `.module.ts` postfix.

```typescript
import { IModule } from 'redux-dynamic-modules';
import { Reducer } from 'redux';
import { myReducer, MyRootStateType } from './my.reducer';

export function getMyModule(): IModule<MyRootStateType> {
  return {
    id: 'my',
    reducerMap: {
      my: myReducer as Reducer,
    },
  };
}

export default getMyModule;
```

Using this module in `React` is a simple as wrapping it around your component using the `DynamicModuleLoader` component.

```javascript
import React from 'react';
import { DynamicModuleLoader } from 'redux-dynamic-modules-react';
import { getMyModule } from './store/my.module';

const MyModule: React.FunctionComponent<{}> = () => (
  <DynamicModuleLoader modules={[getMyModule()]}>
    <h1>Hello world</h1>
  </DynamicModuleLoader>
);

export default MyModule;
```

## Activating middleware

> You have no influence in the order of cross modular middlewares.

If your module requires middleware to handle certain side effects you can activate the middleware from the module entrypoint.

```typescript
import { IModule } from 'redux-dynamic-modules';
import { Reducer } from 'redux';
import { myReducer, MyRootStateType } from './my.reducer';
import { myMiddleware } from './my.middleware';

export function getMyModule(): IModule<MyRootStateType> {
  return {
    id: 'my',
    reducerMap: {
      my: myReducer as Reducer,
    },
    middlewares: [myMiddleware],
  };
}

export default getMyModule;
```

## Firing initial actions

If you need to fire an action the moment the module gets mounted you can.

```typescript
import { IModule } from 'redux-dynamic-modules';
import { Reducer } from 'redux';
import { myReducer, MyRootStateType } from './my.reducer';
import { myAction } from './my.actions';

export function getMyModule(): IModule<MyRootStateType> {
  return {
    id: 'my',
    reducerMap: {
      my: myReducer as Reducer,
    },
    initialActions: [myAction()],
  };
}

export default getMyModule;
```
